//
//  ViewController.swift
//  NYCHighSchoolApp
//
//  Created by Sunil Kumar Somavarapu on 7/8/22.
//

import UIKit

class ViewController: UIViewController {
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
        
    var schoolInformation = [SchoolInformation]()
    var satScores = [SATInformationModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableView()
        fetchSchoolInformation()
        fetchSATInformation()
        buildView()
    }
    
    // MARK: Register Table View
    
    private func registerTableView() {
        tableView.register(SchoolInformationTableViewCell.self,
                           forCellReuseIdentifier: "SchoolInformationTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func buildView() {
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10),
                                     tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10),
                                     tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 10),
                                     tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)])
    }
}

// MARK: Tableview Conforming to DataSource and Delegate

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolInformationTableViewCell", for: indexPath) as! SchoolInformationTableViewCell
        cell.schoolNameLabel.text = schoolInformation[indexPath.row].schoolName
        cell.phoneNumberLabel.text = schoolInformation[indexPath.row].phoneNumber
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schoolInformation.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let satScore = self.satScores[row]
        let satInformationDetailViewController = SATInformationDetailViewController(satScore: satScore)

        let navigationController = UINavigationController(rootViewController: satInformationDetailViewController)
        satInformationDetailViewController.title = "School \(row + 1)"
        present(navigationController, animated: true)
    }
}

// MARK: Fetching School Information - URLSession

extension ViewController {
    func fetchSchoolInformation() {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        let url = URL(string: urlString)
        guard let url = url else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            do {
                let data = try JSONDecoder().decode([SchoolInformation].self, from: data)
                DispatchQueue.main.async {
                    self.schoolInformation = data
                    self.tableView.reloadData()
                }
            } catch let error {
                print("Error serializing JSON:", error)
            }
        }.resume()
    }
}

// MARK: Fetching SAT Information - URLSession

extension ViewController {
    func fetchSATInformation() {
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        let url = URL(string: urlString)
        guard let url = url else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            do {
                let data = try JSONDecoder().decode([SATInformationModel].self, from: data)
                DispatchQueue.main.async {
                    self.satScores = data
                }
            } catch let error {
                print("Error serializing JSON:", error)
            }
        }.resume()
    }
}
