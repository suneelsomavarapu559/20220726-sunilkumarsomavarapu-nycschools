//
//  SATInformationDetailViewController.swift
//  NYCHighSchoolApp
//
//  Created by Sunil Kumar Somavarapu on 7/9/22.
//

import Foundation
import UIKit

class SATInformationDetailViewController: UIViewController {
    public lazy var satInformationView = SATInformationView()
    var satScore: SATInformationModel = SATInformationModel()
    
    init(satScore: SATInformationModel) {
        self.satScore = satScore
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        bindView()
    }
    
    override open func loadView() {
        view = satInformationView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func bindView() {
        satInformationView.satReadingScoreLabel.text = "SAT Reading Score: \(satScore.satReadingScore)"
        satInformationView.satMathScoreLabel.text = "SAT Math Score: \(satScore.satMathScore)"
        satInformationView.satWritingScoreLabel.text = "SAT Writing Score: \(satScore.satWritingScore)"
        satInformationView.satAverageScoreLabel.text = "SAT Average Score: \(satScore.satAverageScore)"
        satInformationView.schoolDbnCodeLabel.text = "School DBN Code: \(satScore.schoolDbnCode)"
    }
    
    @objc private func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
}
