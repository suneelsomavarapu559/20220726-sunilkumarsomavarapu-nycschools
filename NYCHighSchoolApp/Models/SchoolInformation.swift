//
//  SchoolInformation.swift
//  NYCHighSchoolApp
//
//  Created by Sunil Kumar Somavarapu on 7/8/22.
//

import Foundation

struct SchoolInformation {
    let schoolName: String
    let phoneNumber: String
}

extension SchoolInformation: Decodable {
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case phoneNumber = "phone_number"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        schoolName = try container.decode(String.self, forKey: .schoolName)
        phoneNumber = try container.decode(String.self, forKey: .phoneNumber)
    }
}
