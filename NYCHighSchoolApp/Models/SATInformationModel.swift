//
//  SATInformationModel.swift
//  NYCHighSchoolApp
//
//  Created by Sunil Kumar Somavarapu on 7/9/22.
//

import Foundation

struct SATInformationModel {
    let satReadingScore: Int
    let satMathScore: Int
    let satWritingScore: Int
    let satAverageScore: Int
    let schoolDbnCode: String
    
    public init(satReadingScore: Int = 0,
                satMathScore: Int = 0,
                satWritingScore: Int = 0,
                satAverageScore: Int = 0,
                schoolDbnCode: String = "") {
        self.satReadingScore = satReadingScore
        self.satMathScore = satMathScore
        self.satWritingScore = satWritingScore
        self.satAverageScore = satAverageScore
        self.schoolDbnCode = schoolDbnCode
    }
}

extension SATInformationModel: Decodable {
    enum CodingKeys: String, CodingKey {
        case satReadingScore = "sat_critical_reading_avg_score"
        case satMathScore = "sat_math_avg_score"
        case satWritingScore = "sat_writing_avg_score"
        case schoolDbnCode = "dbn"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let readingScore = try container.decode(String.self, forKey: .satReadingScore)
        let mathScore = try container.decode(String.self, forKey: .satMathScore)
        let writingScore = try container.decode(String.self, forKey: .satWritingScore)
        schoolDbnCode = try container.decode(String.self, forKey: .schoolDbnCode)
        
        guard let satReadingScore = Int(readingScore),
                let satMathScore = Int(mathScore),
                let satWritingScore = Int(writingScore) else {
            satReadingScore = 0
            satMathScore = 0
            satWritingScore = 0
            satAverageScore = 0
            return
        }
        
        self.satReadingScore = satReadingScore
        self.satMathScore = satMathScore
        self.satWritingScore = satWritingScore
        self.satAverageScore = (satReadingScore + satMathScore + satWritingScore) / 3
    }
}
