//
//  SATInformationView.swift
//  NYCHighSchoolApp
//
//  Created by Sunil Kumar Somavarapu on 7/9/22.
//

import Foundation
import UIKit

class SATInformationView: UIView {
    private lazy var satInformationVerticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var satReadingScoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Suneel"
        return label
    }()
    
    lazy var satMathScoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Suneel"
        return label
    }()
    
    lazy var satWritingScoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Suneel"
        return label
    }()
    
    lazy var dashLabel: UILabel = {
        let label = UILabel()
        label.text = "---------------------------"
        return label
    }()
    
    lazy var hyphenLabel: UILabel = {
        let label = UILabel()
        label.text = "---------------------------"
        return label
    }()
    
    lazy var satAverageScoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Suneel"
        return label
    }()
    
    lazy var schoolDbnCodeLabel: UILabel = {
        let label = UILabel()
        label.text = "29M929"
        return label
    }()
    
    private lazy var spacerView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildView() {
        self.addSubview(satInformationVerticalStackView)
        satInformationVerticalStackView.addArrangedSubviews(satReadingScoreLabel,
                                                            satMathScoreLabel,
                                                            satWritingScoreLabel,
                                                            hyphenLabel,
                                                            satAverageScoreLabel,
                                                            dashLabel,
                                                            schoolDbnCodeLabel,
                                                            spacerView)
        
        NSLayoutConstraint.activate([satInformationVerticalStackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                                     satInformationVerticalStackView.rightAnchor.constraint(equalTo: self.rightAnchor),
                                     satInformationVerticalStackView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
                                     satInformationVerticalStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor)])
        
        NSLayoutConstraint.activate([satReadingScoreLabel.widthAnchor.constraint(equalToConstant: 200),
                                     satReadingScoreLabel.heightAnchor.constraint(equalToConstant: 40)])
        
        NSLayoutConstraint.activate([satMathScoreLabel.widthAnchor.constraint(equalToConstant: 200),
                                     satMathScoreLabel.heightAnchor.constraint(equalToConstant: 40)])
        
        NSLayoutConstraint.activate([satWritingScoreLabel.widthAnchor.constraint(equalToConstant: 200),
                                     satWritingScoreLabel.heightAnchor.constraint(equalToConstant: 40)])
        
        NSLayoutConstraint.activate([satAverageScoreLabel.widthAnchor.constraint(equalToConstant: 200),
                                     satAverageScoreLabel.heightAnchor.constraint(equalToConstant: 40)])
        
        NSLayoutConstraint.activate([schoolDbnCodeLabel.widthAnchor.constraint(equalToConstant: 200),
                                     schoolDbnCodeLabel.heightAnchor.constraint(equalToConstant: 40)])
    }
}

extension UIStackView {
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach { view in
            addArrangedSubview(view)
        }
    }
}
