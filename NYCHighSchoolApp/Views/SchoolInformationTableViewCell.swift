//
//  SchoolInformationTableViewCell.swift
//  NYCHighSchoolApp
//
//  Created by Sunil Kumar Somavarapu on 7/8/22.
//

import Foundation
import UIKit

class SchoolInformationTableViewCell: UITableViewCell {
    private lazy var schoolInformationVerticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var schoolNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Suneel"
        return label
    }()
    
    lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "281-818-1909"
        return label
    }()
    
    lazy var showDetailsButton: UIButton = {
        let button = UIButton()
        button.setTitle("Show Details", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        return button
    }()
            
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildView() {
        self.addSubview(schoolInformationVerticalStackView)
        
        schoolInformationVerticalStackView.addArrangedSubview(schoolNameLabel)
        schoolInformationVerticalStackView.addArrangedSubview(phoneNumberLabel)
        schoolInformationVerticalStackView.addArrangedSubview(showDetailsButton)
        
        NSLayoutConstraint.activate([schoolInformationVerticalStackView.leftAnchor.constraint(equalTo: self.leftAnchor),
                                     schoolInformationVerticalStackView.rightAnchor.constraint(equalTo: self.rightAnchor),
                                     schoolInformationVerticalStackView.topAnchor.constraint(equalTo: self.topAnchor),
                                     schoolInformationVerticalStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor)])
        
        NSLayoutConstraint.activate([schoolNameLabel.widthAnchor.constraint(equalToConstant: 200),
                                     schoolNameLabel.topAnchor.constraint(equalTo: schoolInformationVerticalStackView.topAnchor, constant: 20)])
    }
}
